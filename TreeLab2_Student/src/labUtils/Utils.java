package labUtils;

import java.util.Iterator;

import treeClasses.LinkedBinaryTree;
import treeClasses.LinkedTree;
import treeInterfaces.Position;
import treeInterfaces.Tree;

public class Utils {
	public static <E> void displayTree(String msg, Tree<E> t) { 
		System.out.println("\n\n" + msg); 
		t.display();
	}

	public static <E> void displayTreeElements(String msg, Tree<E> t) {
		System.out.println("\n\n" + msg); 
		for (E element : t)
			System.out.println(element); 
		
	}
	
	public static LinkedTree<Integer> buildExampleTreeAsLinkedTree() { 
		LinkedTree<Integer> t = new LinkedTree<>(); 

		t.addRoot(4);
		
		Position<Integer> nine = t.addChild(t.root(), 9);
		Position<Integer> twenty = t.addChild(t.root(), 20);
		Position<Integer> fifteen = t.addChild(twenty, 15);
		Position<Integer> twenty1 = t.addChild(twenty, 21);
		
		t.addChild(fifteen, 12);

		Position<Integer> seventeen = t.addChild(fifteen, 17);
		Position<Integer> fourty = t.addChild(twenty1, 40);


		t.addChild(nine, 7);
		t.addChild(nine, 10);
		t.addChild(seventeen, 19);
		t.addChild(fourty, 30);
		t.addChild(fourty, 45);

		return t; 
	}
	
	public static LinkedBinaryTree<Integer> buildExampleTreeAsLinkedBinaryTree() { 
		LinkedBinaryTree<Integer> t = new LinkedBinaryTree<>(); 
		
		t.addRoot(4);
		
		Position<Integer> nine = t.addLeft(t.root(), 9);
		Position<Integer> twenty = t.addRight(t.root(), 20);
		Position<Integer> fifteen = t.addLeft(twenty, 15);
		Position<Integer> twenty1 = t.addRight(twenty, 21);
		Position<Integer> seventeen = t.addRight(fifteen, 17);
		Position<Integer> fourty = t.addRight(twenty1, 40);

		t.addLeft(fifteen, 12);
		t.addLeft(nine, 7);
		t.addRight(nine, 10);
		t.addLeft(seventeen, 19);
		t.addLeft(fourty, 30);
		t.addRight(fourty, 45);
		
		return t; 
	}
	
	public static <E> void displayIter(String msg, Iterator<E> iter) { 
	    System.out.println(msg); 
	    while (iter.hasNext()) 
	         System.out.println(iter.next()); 
	} 

	public static LinkedTree<Character> buildTrieAsLinkedTree() { 
		   String[] words = {"sal", "sala", "salado", "salto", "si", "u", "un", "uno"}; 

		   
//		   String[] words = {"aleluya", "aleluyado", "aleta", "aletas", "alerta", 
//		        "alertado", "altercado", "altercados", "altura", "alturo", "alturito", 
//		         "balacera", "bala", "balas", "bala", "balon", "barato"};
		   
		        
		   LinkedTree<Character> t = new LinkedTree<>(); 
		   Position<Character> p = t.addRoot('['); 
		   for (String s : words)
		    addWordToTrie(t, p, s, 0);   // auxiliary method below
		            
		   return t; 
		}
		    
		private static void addWordToTrie(LinkedTree<Character> t, 
		        Position<Character> r, String s, int index) {
		   if (index == s.length()) 
		    t.addChild(r,  ']'); 
		   else { 
		      Position<Character> pcc = null;   // position in children of r containing s[index] 
		    for (Position<Character> p : t.children(r))
		       if (p.getElement().equals(s.charAt(index))) pcc = p; 
		    if (pcc == null)
		       r = t.addChild(r, s.charAt(index)); 
		    else 
		       r = pcc;
		    addWordToTrie(t, r, s, index+1); 

		   }
		}



}
